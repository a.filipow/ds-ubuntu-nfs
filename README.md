# ds-ubuntu-nfs

Docker Swarm Cluster
- Ubuntu 18.04
- NFS-Storage


# preparation

- cd / && git clone https://gitlab.com/a.filipow/ds-ubuntu-nfs.git
- chmod 700 /ds-nfs/prepare-node.sh && cd /ds-nfs/ && ./prepare-node.sh
- rm -rf /ds-nfs
- create image from instance

# deployment

- cd / && git clone https://gitlab.com/a.filipow/ds-ubuntu-nfs.git

set environment variables
- export flippink_nfs_url=


- chmod 700 /ds-nfs/prepare-swarm.sh && cd /ds-nfs/ && ./prepare-swarm.sh


set cronjobs and custom startup service


Configure each compose file
- nano /ds-nfs/loadbalancer/docker-compose.yml
- nano /ds-nfs/dashboard/docker-compose.yml
- nano /ds-nfs/monitoring/docker-compose.yml

- chmod 700 /ds-nfs/create-swarm.sh && cd /ds-nfs/ && ./create-swarm.sh

# Adding Nodes to Cluster

use following script at lauch to auto join the cluster:

---SCRIPT START---

#!/bin/bash

sudo -i

mkdir /nfs

mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport #NFS-URL-HERE#:/  /nfs

cd /nfs

chmod go+rw .

# JOIN COMMAND #

---SCRIPT END---